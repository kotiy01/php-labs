<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="styles.css">
    <title>Feedback</title>
  </head>
  <body>
    <header class="header">
      <img src="logo.png" alt="logo" class="header__logo">
      <p class="header__title">Feedback Form</p>
    </header>
    <main class="main">
      <div class="feedback-container">
        <?php
          $url = 'https://httpbin.org';
          $answer = get_headers($url);
        ?>
        <textarea type="text" name="feedback" class="feedback-container__answer">
          <?php
            print_r($answer);
          ?>
        </textarea>
      </div>
    </main>
    <footer class="footer">
      <p class="footer__text">Собрать сайт из двух страниц.<br>
        1 страница: Сверстать форму обратной связи. Отправку формы осуществить на URL: https://httpbin.org/post. Добавить кнопку для перехода на 2 страницу.<br>
        2 страница: вывести на страницу результат работы функции get_headers.<br>
        Загрузить код в удаленный репозиторий. Залить на хостинг.</p>
    </footer>
  </body>
</html>
