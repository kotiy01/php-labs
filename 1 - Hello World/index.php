<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Hello World</title>
    <style media="screen">
      * {
        margin: 0;
        padding: 0;
      }
      body {
        width: 100%;
        background: rgba(56, 187, 201, 1.0);
        background: linear-gradient(to bottom right, rgba(56, 187, 201, 1.0), rgba(230, 80, 139, 1.0));
      }
      header {
        display: flex;
        align-items: center;
        justify-content: flex-start;
        color: white;
        height: 100px;
        width: 100%;
        background: rgba(0, 0, 0, 0.5);
        position: fixed;
      }
      main{
        display: flex;
        align-items: center;
        justify-content: center;
        height: 100vh;
        color: white;
      }
      footer {
        display: flex;
        align-items: center;
        justify-content: center;
        width: 100%;
        color: white;
        height: 100px;
        background: rgba(0, 0, 0, 0.5);
        bottom: 0;
        position: fixed;
      }
      p {
        font-size: 18px;
      }
      h1 {
        font-size: 24px;
      }
      .header-text {
        position: absolute;
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
      }
    </style>
  </head>
  <body>
    <header>
      <img src="logo.png" alt="logo">
      <p class="header-text">Hello, PHP!</p>
    </header>
    <main>
        <?php echo "<h1>Hello, World!</h1>"; ?>
    </main>
    <footer>
      <p>Создать веб-страницу с динамическим контентом. Загрузить код в удаленный репозиторий. Залить на хостинг.</p>
    </footer>
  </body>
</html>
